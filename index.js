//bài 1..............................
var Sum1 = 0;
var n = 0;
while (Sum1 < 10000) {
  n++;
  Sum1 = Sum1 + n;
}
document.querySelector(
  ".result1"
).innerHTML = ` Số nguyên dương nhỏ nhất là: ${n}`;
// bài 2
document.querySelector("#btn2").onclick = function () {
  // input
  var numX = document.querySelector("#numX").value * 1;
  var numY = document.querySelector("#numY").value * 1;
  // output
  var ketqua2 = 0;
  // xử lý
  for (var i2 = 1; i2 <= numY; i2++) {
    ketqua2 += Math.pow(numX, i2);
  }
  document.querySelector(".result2").innerHTML = ` Tổng: ${ketqua2}`;
};
// bài 3:

document.querySelector("#btn3").onclick = function () {
  // input
  var sogiaithua = document.querySelector("#num3").value * 1;
  // output
  var ketqua3 = 1;
  // xử lý
  for (var i = 1; i <= sogiaithua; i++) {
    ketqua3 *= i;
  }
  document.querySelector(".result3").innerHTML = ` Giai thừa là: ${ketqua3}`;
};
// bài 4
// output
var ketqua4 = "";
document.querySelector("#btn4").onclick = function () {
  for (i = 1; i <= 10; i++) {
    if (i % 2 === 0) {
      ketqua4 += `<div class='bg-danger'>DIV CHẲN</div>`;
    } else {
      ketqua4 += `<div class='bg-primary'>DIV LẺ</div>`;
    }
  }
  document.querySelector(".result4").innerHTML = ketqua4;
};
// bài 5
document.querySelector("#btn5").onclick = function () {
  // input
  var number = document.querySelector("#num5").value * 1;
  // output
  var ketqua5 = "";
  for (var i = 2; i <= number; i++) {
    var checksnt = true;
    for (j = 2; j <= Math.sqrt(i); j++) {
      if (i % j === 0) {
        checksnt = false;
        break;
      }
    }
    if (checksnt) {
      ketqua5 += i + " ";
    }
  }

  document.querySelector(".result5").innerHTML = ketqua5;
};
